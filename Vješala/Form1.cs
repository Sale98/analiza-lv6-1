﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vješala
{
    public partial class Form1 : Form
    {
        int pokusaji = 9;
        int brojac = 0;
        string Rijeci = "";
        class Vjesala
        {
            #region data_members
            private string rijec;
            #endregion
            #region public_methods
            public Vjesala()
            {
                rijec="Slovo";
            }
            public Vjesala(string a)
            {
                rijec = a;
            }
            public override string ToString()
            {
                return rijec.ToString();
            }
            #endregion
        }
        List<Vjesala> listaVjesala = new List<Vjesala>();
        string path = "C:\\rijeci.txt";
        static Random rand = new Random();
        public Form1()
        {
            InitializeComponent();
        }
            private void Form1_Load(object sender, EventArgs e)
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null) 
                    {
                        string parts= line;
                        Vjesala V= new Vjesala(parts);
                        listaVjesala.Add(V);
                    }
                    listBox1.DataSource=null;
                    listBox1.DataSource=listaVjesala;
                }
            }

            private void button2_Click(object sender, EventArgs e)
            {
                label2.Text = "";
                label1.Text = pokusaji.ToString();
                pokusaji = 9;
                int i = rand.Next(listaVjesala.Count);
                Rijeci = listaVjesala[i].ToString();
                for(int n=0;n<Rijeci.Length;n++)
                {
                    label2.Text += "_";
                }
            }

            private void button1_Click(object sender, EventArgs e)
            {
                string c="";
                int i;
                if(textBox1.Text=="")
                {
                    pokusaji--;
                    label1.Text = pokusaji.ToString();
                    textBox1.Text = "";
                    if(pokusaji==0)
                    {
                        MessageBox.Show("Kraj, niste pogodili");
                    }
                }
                else
                {
                    for(i=0;i<Rijeci.Length;i++)
                    {
                        if(c[0]==Rijeci[i])
                        {
                           label2.Text=label1.Text.Remove(i,1);
                           label2.Text = label1.Text.Insert(i, c);
                        }
                    }

                    if(brojac==Rijeci.Length)
                    {
                        MessageBox.Show("Pogodili ste");
                    }
                    textBox1.Text = "";
                }
            }
       }
}
